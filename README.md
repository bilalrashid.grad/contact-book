# Contact-Book
A contact book built in Flask

# Dependencies
- Python
- [flask](http://flask.pocoo.org/)
- flask-restful
- Pip
- SQlite


# Instructions
- Download everything
- Put it in a folder called API
- Make sure all dependecies are installed 
- Do python CreateTable.py to set up a new database
- The file to run is app.py 
- The file for unit testing is test.py
- Enjoy
